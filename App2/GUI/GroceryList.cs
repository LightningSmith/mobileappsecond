﻿using Android.App;
using Android.OS;

namespace App2.GUI
{
    [Activity(Label = "GroceryList")]
    public class GroceryList : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.GroceryList);
        }
    }
}